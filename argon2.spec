Name:          argon2
Version:       20190702
Release:       4
Summary:       A password-hashing tool
License:       Public Domain or ASL 2.0
URL:           https://github.com/P-H-C/phc-winner-argon2
Source0:       https://github.com/P-H-C/phc-winner-argon2/archive/62358ba2123abd17fccf2a108a301d4b52c01a7c/phc-winner-argon2-20190702-62358ba.tar.gz

Patch1:  backport-fix-3-spelling-mistakes.patch
Patch2:  backport-Don-t-fail-on-existing-symlink.patch

BuildRequires: gcc
Requires:      libargon2%{?_isa} = %{version}-%{release}

%description
Argon2 is a password-hashing function that summarizes the state of the art in the design of 
memory-hard functions and can be used to hash passwords for credential storage, key derivation, 
or other applications.

It has a simple design aimed at the highest memory filling rate and effective use of multiple 
computing units, while still providing defense against tradeoff attacks (by exploiting the 
cache and memory organization of the recent processors).

Argon2 has three variants: Argon2i, Argon2d, and Argon2id. Argon2d is faster and uses data-depending 
memory access, which makes it highly resistant against GPU cracking attacks and suitable for 
applications with no threats from side-channel timing attacks (eg. cryptocurrencies). Argon2i 
instead uses data-independent memory access, which is preferred for password hashing and 
password-based key derivation, but it is slower as it makes more passes over the memory to 
protect from tradeoff attacks. Argon2id is a hybrid of Argon2i and Argon2d, using a combination 
of data-depending and data-independent memory accesses, which gives some of Argon2i's resistance 
to side-channel cache timing attacks and much of Argon2d's resistance to GPU cracking attacks.

Argon2i, Argon2d, and Argon2id are parametrized by:

- A time cost, which defines the amount of computation realized and therefore the execution 
time, given in number of iterations
- A memory cost, which defines the memory usage, given in kibibytes
- A parallelism degree, which defines the number of parallel threads

%package -n libargon2-devel
Summary:       Development library files for argon2
Requires:      libargon2%{?_isa} = %{version}-%{release}
Obsoletes:     argon2-devel < %{version}-%{release}

%description -n libargon2-devel
This package contains development library files and headers for the argon2.

%package -n libargon2
Summary:       The password-hashing library for argon2

%description -n libargon2
This package contains dynamic library for argon2

%package_help

%prep
%autosetup -n phc-winner-argon2-62358ba2123abd17fccf2a108a301d4b52c01a7c -p1

if ! grep -q 'ABI_VERSION = 1' Makefile; then
  : soname have changed
  grep soname Makefile
  exit 1
fi

sed -e 's:lib/@HOST_MULTIARCH@:%{_lib}:;s/@UPSTREAM_VER@/%{version}/' -i libargon2.pc.in

sed -e '/^CFLAGS/s:^CFLAGS:LDFLAGS=%{build_ldflags}\nCFLAGS:' \
    -e 's:-O3 -Wall:%{optflags}:' \
    -e '/^LIBRARY_REL/s:lib:%{_lib}:' \
    -e 's:-march=\$(OPTTARGET) :${CFLAGS} :' \
    -e 's:CFLAGS += -march=\$(OPTTARGET)::' \
    -i Makefile

sed -e 's/\/x86_64-linux-gnu//g' -i Makefile

%build
make -j1

%install
%make_install
%delete_la_and_a

install -Dpm 755 libargon2.so.* %{buildroot}%{_libdir}/
install -Dpm 644 libargon2.pc %{buildroot}%{_libdir}/pkgconfig/libargon2.pc
install -Dpm 644 man/argon2.1 %{buildroot}%{_mandir}/man1/argon2.1

%check
make test

%pre

%preun

%post -n libargon2 -p /sbin/ldconfig

%postun -n libargon2 -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%license LICENSE

%files -n libargon2
%defattr(-,root,root)
%license LICENSE
%{_libdir}/libargon2.so.*

%files -n libargon2-devel
%defattr(-,root,root)
%{_includedir}/%{name}.h
%{_libdir}/libargon2.so
%{_libdir}/pkgconfig/*pc

%files help
%doc *md
%{_mandir}/man1/*

%changelog
* Wed Jul 31 2024 yixiangzhike <yixiangzhike007@163.com> - 20190702-4
- backport upstream patch to fix failure on existing symlink

* Tue Oct 18 2022 yixiangzhike <yixiangzhike007@163.com> - 20190702-3
- fix spelling mistakes

* Tue Apr 12 2022 yixiangzhike <yixiangzhike007@163.com> - 20190702-2
- delete the old version .so file

* Tue Sep 1 2020 zhangxingliang <zhangxingliang3@huawei.com> - 20190702-1
- update to 20190702

* Mon Jul 20 2020 Liquor <lirui130@huawei.com> - 20171227-1
- update to 20171227

* Mon Jul 6 2020 Liquor <lirui130@huawei.com> - 20161029-10
- revert "update argon2 to 20190702" 

* Sat Jul 4 2020 Liquor <lirui130@huawei.com> - 20190702-2
- add libargon2.so.0

* Fri Jul 3 2020 wangchen <wangchen137@huawei.com> - 20190702-1
- update argon2 to 20190702

* Sat Mar 21 2020 openEuler Buildteam <buildteam@openeuler.org> - 20161029-9
- obsoletes argon2-devel

* Mon Feb 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 20161029-8
- add libargon2 containing dynamic library for argon2; change argon2-devel to libargon2-devel

* Tue Sep 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 20161029-7
- Package init
